#include "game_gui.h"

#include <curses.h>

void gui_init(void)
{
    initscr();
    cbreak();
    noecho();
    keypad(stdscr, TRUE);
    curs_set(0);
    nodelay(stdscr, TRUE);
    const char game_title[] = "Conway's Game of Life (press 'q' to quit)";
    mvprintw(0, COLS / 2 - sizeof(game_title) / 2, game_title);
    refresh();
}

void gui_destroy(void)
{
    curs_set(1);
    echo();
    nocbreak();
    endwin();
}

void gui_draw_universe(cell_type **universe, size_t height, size_t width)
{
    size_t drawing_height, drawing_width;
    size_t i, j;
    const size_t heigth_offset = 1;
    drawing_height = height > LINES - heigth_offset ? LINES : height;
    drawing_width = width > COLS ? COLS : width;

    for (i = heigth_offset; i < drawing_height; i++)
    {
        for (j = 0; j < drawing_width; j++)
        {
            if (universe[i][j + 1] == CELL_ALIVE) mvaddch(i, j, GUI_CELL_ALIVE);
            else if (universe[i][j + 1] == CELL_DEAD) mvaddch(i, j, GUI_CELL_DEAD);
            else mvaddch(i, j, GUI_CELL_BORDER);
        }

    }

    refresh();
}
