#ifndef _UNIVERSE_H_
#define _UNIVERSE_H_

#include "cell.h"
#include <stdlib.h>

cell_type **allocate_universe(size_t height, size_t width);

void delete_universe(cell_type **universe, size_t height);

cell_type **clone_universe(cell_type **universe, size_t height, size_t width);

void init_universe(cell_type **universe, size_t height, size_t width);

int load_universe(char *filepath, cell_type **universe, size_t height, size_t width);

#endif
