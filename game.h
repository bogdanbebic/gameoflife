#ifndef _GAME_H_
#define _GAME_H_

#include <stdlib.h>
#include "cell.h"

unsigned int count_alive_neighbors(cell_type **universe, size_t i, size_t j);

void evolve(cell_type **universe, size_t height, size_t width);

void game(char *universe_filepath, size_t height, size_t width);

#define DEFAULT_HEIGHT  1000
#define DEFAULT_WIDTH   1000

#endif
