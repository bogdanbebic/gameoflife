height = 50
width = 50
separator_width = ' '
separator_height = '\n'

with open("test_map.txt", "w") as file:
    file.write(f"{height} {width}\n")
    for i in range(height):
        for j in range(width):
            file.write("0")
            if j != width - 1:
                file.write(separator_width)
        file.write(separator_height)
    pass
