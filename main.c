#include "game.h"

#include <stdio.h>
#include <time.h>

int main(int argc, char const *argv[])
{
    char *filepath  = NULL;
    size_t height   = DEFAULT_HEIGHT;
    size_t width    = DEFAULT_WIDTH;

    // TODO: parse cmd args
    if (argc == 4)
    {
        filepath    = (char *)argv[1];
        height      = atoi(argv[2]);
        width       = atoi(argv[3]);
    } else if (argc != 1)
    {
        fprintf(stderr, "Error: invalid usage.\nUsage: life [<filepath> <height> <width>]\n");
        return -1;
    }

    srand(time(NULL));
    game(filepath, height, width);
    return 0;
}
