#include "game.h"
#include "cell.h"
#include "universe.h"
#include "game_gui.h"

#include <unistd.h>

#include <curses.h>

static int is_alive(cell_type cell)
{
    return cell == CELL_ALIVE;
}

unsigned int count_alive_neighbors(cell_type **universe, size_t i, size_t j)
{
    unsigned int alive_neighbors = 0;

    if (is_alive(universe[i - 1][j - 1]))   alive_neighbors++;
    if (is_alive(universe[i - 1][j]))       alive_neighbors++;
    if (is_alive(universe[i - 1][j + 1]))   alive_neighbors++;

    if (is_alive(universe[i][j - 1]))       alive_neighbors++;
    if (is_alive(universe[i][j]))           alive_neighbors++;
    if (is_alive(universe[i][j + 1]))       alive_neighbors++;
    
    if (is_alive(universe[i + 1][j - 1]))   alive_neighbors++;
    if (is_alive(universe[i + 1][j]))       alive_neighbors++;
    if (is_alive(universe[i + 1][j + 1]))   alive_neighbors++;

    return alive_neighbors;
}

void evolve(cell_type **universe, size_t height, size_t width)
{
    size_t i, j;
    cell_type **next_universe = clone_universe(universe, height, width);
    
    if (next_universe == NULL) return;  // Memory exception
    
    for (i = 1; i < height - 1; i++)
    {
        for (j = 1; j < width - 1; j++)
        {
            unsigned int alive_neighbors = count_alive_neighbors(universe, i, j);

            if (alive_neighbors == 3)       universe[i][j] = CELL_ALIVE;
            else if (alive_neighbors != 4)  universe[i][j] = CELL_DEAD;

        }

    }

    delete_universe(next_universe, height);
}

void game(char *universe_filepath, size_t height, size_t width)
{
    cell_type **universe = allocate_universe(height, width);
    if (universe == NULL)
    {
        return;
    }

    if (universe_filepath == NULL) init_universe(universe, height, width);
    else if (load_universe(universe_filepath, universe, height, width) != 0)
    {
        fprintf(stderr, "Error: no such file\n");
        delete_universe(universe, height);
        return;
    }
    
    gui_init();

    while (getch() != 'q')
    {
        evolve(universe, height, width);
        gui_draw_universe(universe, height, width);
        // sleep(1);
    }
    
    gui_destroy();

    delete_universe(universe, height);
}
