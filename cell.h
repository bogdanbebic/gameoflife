#ifndef _CELL_H_
#define _CELL_H_

typedef int cell_type;

#define CELL_DEAD   0
#define CELL_ALIVE  1
#define CELL_BORDER 2

#endif
