#include "universe.h"

#include <stdlib.h>

cell_type **allocate_universe(size_t height, size_t width)
{
    size_t i;
    cell_type **universe = malloc(height * sizeof(cell_type*));
    int success_flag = 1;
    if (universe == NULL)
    {
        return NULL;
    }

    for (i = 0; i < height; i++)
    {
        int j;
        universe[i] = malloc(width * sizeof(cell_type));
        if (universe[i] == NULL)
        {
            success_flag = 0;
        }

    }

    if (!success_flag)
    {
        delete_universe(universe, height);
        universe = NULL;
    }

    return universe;
}

void delete_universe(cell_type **universe, size_t height)
{
    size_t i;
    for (i = 0; i < height; i++)
    {
        free(universe[i]);
    }

    free(universe);
}

cell_type **clone_universe(cell_type **universe, size_t height, size_t width)
{
    size_t i, j;
    cell_type **cloned_universe = allocate_universe(height, width);
    if (cloned_universe == NULL || universe == NULL)
    {
        return NULL;
    }

    for (i = 0; i < height; i++)
        for (j = 0; j < width; j++)
            cloned_universe[i][j] = universe[i][j];
    
    return cloned_universe;
}

void init_universe(cell_type **universe, size_t height, size_t width)
{
    size_t i, j;
    for (i = 1; i < height - 1; i++)
        for (j = 1; j < width - 1; j++)
            universe[i][j] = rand() % 5 ? CELL_ALIVE : CELL_DEAD;

    for (i = 0; i < height; i++)
    {
        universe[i][0]          = CELL_BORDER;
        universe[i][width - 1]  = CELL_BORDER;
    }

    for (j = 0; j < width; j++)
    {
        universe[0][j]          = CELL_BORDER;
        universe[height - 1][j] = CELL_BORDER;
    }

}

int load_universe(char *filepath, cell_type **universe, size_t height, size_t width)
{
    // TODO: implement
    return -1;
}
