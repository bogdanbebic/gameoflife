#ifndef _GAME_GUI_H_
#define _GAME_GUI_H_

#include <stdlib.h>
#include "cell.h"

void gui_init(void);

void gui_destroy(void);

void gui_draw_universe(cell_type **universe, size_t height, size_t width);

#define GUI_CELL_ALIVE  219
#define GUI_CELL_DEAD   ' '
#define GUI_CELL_BORDER '*'

#endif
